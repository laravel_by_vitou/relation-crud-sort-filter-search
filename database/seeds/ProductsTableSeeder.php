<?php

use Illuminate\Database\Seeder;
class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = New App\Product();
        $product->name = str_random('5');
        $product->user_id = '2';
        $product->save();
        $category = App\Category::find([1,2]);
        $product->categories()->attach($category);
    }
}
