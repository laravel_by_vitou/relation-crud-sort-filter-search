<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'admin',
            'email'=>'admin@admin.com',
            'password'=>bcrypt('secret'),
            'admin'=>'1'
        ]);
        DB::table('users')->insert([
            'name'=>str_random(10),
            'email'=>str_random(10).'@gmai.com',
            'password'=>bcrypt('secret')
        ]);
        DB::table('users')->insert([
            'name'=>str_random(10),
            'email'=>str_random(10).'@gmai.com',
            'password'=>bcrypt('secret')
        ]);
        DB::table('users')->insert([
            'name'=>str_random(10),
            'email'=>str_random(10).'@gmai.com',
            'password'=>bcrypt('secret')
        ]);
        DB::table('users')->insert([
            'name'=>str_random(10),
            'email'=>str_random(10).'@gmai.com',
            'password'=>bcrypt('secret')
        ]);
    }
}
