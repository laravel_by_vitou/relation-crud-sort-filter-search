<?php

use Illuminate\Database\Seeder;

class dashbordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dashboards')->insert([
            'p_title'=>str_random(10),
            'p_url'=>str_random(10),
            'user_id'=>1
        ]);
        DB::table('dashboards')->insert([
            'p_title'=>str_random(10),
            'p_url'=>str_random(10),
            'user_id'=>2
        ]);
        DB::table('dashboards')->insert([
            'p_title'=>str_random(10),
            'p_url'=>str_random(10),
            'user_id'=>3
        ]);
        DB::table('dashboards')->insert([
            'p_title'=>str_random(10),
            'p_url'=>str_random(10),
            'user_id'=>4
        ]);
        DB::table('dashboards')->insert([
            'p_title'=>str_random(10),
            'p_url'=>str_random(10),
            'user_id'=>5
        ]);
     
    }
}
