<?php

use Illuminate\Database\Seeder;

class ordersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            'user_id'=> 1,
            'item_id'=> 2
        ]);
        DB::table('orders')->insert([
            'user_id'=> 2,
            'item_id'=> 5
        ]);
        DB::table('orders')->insert([
            'user_id'=> 2,
            'item_id'=> 2
        ]);
      
        DB::table('orders')->insert([
            'user_id'=> 5,
            'item_id'=> 1
        ]);
    }
}
