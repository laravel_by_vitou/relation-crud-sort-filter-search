@extends('layouts.app') 
@section('css')
<style>
    .form-group.required label::after {
        content: " *";
        color: red;
        font-size: bold;
    }
</style>
@endsection
 
@section('content')

<div class=" container">
    <div class="row">
        <div class="col-md-8 col-sm-8 offset-md-2">
            <h1>{{isset($module)?'Edit':'New'}} Module</h1>
            <hr> 
            @if(isset($module)) 
            {!! Form::model($module,['method'=>'put']) !!} 
            @else
             {!! Form::open() !!}
             @endif
            <div class="form-group row required">
                {!! Form::label('p title','',['class'=>'col-form-label cold-md-3 col-lg-2'])!!}
                <div class="col-md-8 ">
                    {!! Form::text("p_title",null,["class"=>"form-control".($errors->has('p_title')?" is-invalid":""),'placeholder'=>'pTitle']) !!} 
                    {!! $errors->first('p_title','<span class="invalid-feedback font-weight-bold">:message</span>')!!}
                </div>
            </div>
            <div class="form-group row required">
                {!! Form::label('p url','',['class'=>'col-form-label cold-md-3 col-lg-2'])!!}
                <div class="col-md-8 ">
                    {!! Form::text("p_url",null,["class"=>"form-control".($errors->has('p_url')?" is-invalid":""),'placeholder'=>'pURL']) !!} 
                    {!! $errors->first('p_url','<span class="invalid-feedback font-weight-bold">:message</span>')!!}
                </div>
            </div>
            <div class="form-group row required">
                {!! Form::label('r title','',['class'=>'col-form-label cold-md-3 col-lg-2'])!!}
                <div class="col-md-8 ">
                    {!! Form::text("r_title",null,["class"=>"form-control".($errors->has('r_title')?" is-invalid":""),'placeholder'=>'rTitle']) !!}
                    {!! $errors->first('r_title','<span class="invalid-feedback font-weight-bold">:message</span>')!!}
                </div>
            </div>
            <div class="form-group row required">
                {!! Form::label('r url','',['class'=>'col-form-label cold-md-3 col-lg-2'])!!}
                <div class="col-md-8 ">
                    {!! Form::text("r_url",null,["class"=>"form-control".($errors->has('r_url')?" is-invalid":""),'placeholder'=>'pURL']) !!} 
                    {!! $errors->first('r_url','<span class="invalid-feedback font-weight-bold">:message</span>')
                    !!}
                </div>
            </div>
            <div class="form-group row ">
                <label for="r_url" class="col-form-label col-md-3 col-sm-3 col-lg-2"></label>
                <div class="col-md-4 ">
                    <a href="{{url('module')}}" class="btn btn-primary">Back</a>
                    <input type="submit" class="btn btn-success">
                </div>
            </div>
            {!! Form::close()!!}
        </div>
    </div>
</div>

@endsection