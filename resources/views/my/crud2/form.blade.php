@extends('layouts/app') 

@section('css')
<style>
    .form-group.required label::after {
        content: " *";
        color: red;
        font-size: bold;
    }
</style>
@endsection
 
@section('content')
<div class="container">
    <div class="col-md-8 offset-md-2">
        <div class="row">
            <h1>{{isset($custom)?'Edit':'New'}} Customer</h1>
        </div>
        <hr> 
        @if(isset($custom)) 
            {!! Form::open($custom,['method'=>'put']) !!} 
        @else 
            {{ Form::open()}} 
                <div class="form-group row required">
                    {{ form::label('name','Name',['class'=>'col-form-label col-md-3 col-lg-2'])}}
                    <div class="col-md-8">
                    {{-- {!! Form::text("name",null,["class"=>"form-control".($errors->has('name')?" is-invalid":""),"autofocus",'placeholder'=>'Name']) !!} --}}
                       
                        {!! Form::text("name",null,["class"=>"form-control".($errors->has('name')?" is-invalid":"" ),"autofocus",'placeholder'=>'Name']) !!}
                        {!! $errors->first('name','<span class="invalid-feedback">:message</span>') !!}
                        {{-- {!! Form::text('name',null,['class'=>'form-control'.( $error->('name')?"is-valid":"" ),'placeholder'=>'Name']) !!} --}}
                    </div>
                </div>
                <div class="form-group row ">
                    {{ form::label('gender','Gender',['class'=>'col-form-label col-md-3 col-lg-2'])}}
                    <div class="col-md-8">
                        {!! Form::select('gender',['Male'=>'Male','Female'=>'Female'],null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="form-group row required">
                    {{ form::label('email','Email',['class'=>'col-form-label col-md-3 col-lg-2'])}}
                    <div class="col-md-8">
                        {!! Form::text('emai',null,['class'=>'form-control'.($errors->has('email')?'is-invalid':''),'placeholder'=>'Email']) !!}
                        {!! $errors->first('email','<span class="invalid-feedback">:message</span>')!!}
                    </div>
                </div>
                <div class="form-group row">
                   <label for="" class="col-md-3 col-lg-2"></label>
                    <div class="col-md-8">
                       <a href="" class="btn btn-danger">Back</a>
                      {!! Form::submit('Save',['class'=>'btn btn-primary']) !!}
                    </div>
                </div>
        @endif 
        {!! Form::close() !!}
    </div>
</div>
@endsection