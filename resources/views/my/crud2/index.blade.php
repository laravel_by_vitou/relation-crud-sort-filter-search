@extends('layouts/app')


@section('content')
    <div class="container">
        <div class="row just-content-between">
            <h1>Customers list (laravel crud, Search, Sort Example)</h1>
            <a href="{{url('/my/laravel-crud-search-sort/create')}}" class="btn btn-primary"> New</a>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4 col-sm">
                <select name="gender" id="gender" class="form-control">
                    <option value="-1">Gender</option>
                    <option value="Male"> Male</option>
                    <option value="Female"> Female</option>
                </select>
            </div>
            <div class="offset-md-2 col-sm">
                {!! Form::open(['method'=>'post']) !!}
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search name">
                        <div class="input-group-btn">
                            <input type="submit" class="btn btn-warning" value="Search">
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="row">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                   <tr>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>
                            <a href="#" class="justify-content-start btn btn-primary">Edit </a>
                            <a href="#" class="justify-content-end btn btn-danger">Delete </a>
                        </td>
                   </tr>
                  
                </tbody>
            </table>
        </div>

    </div>

@endsection