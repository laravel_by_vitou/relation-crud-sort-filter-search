@extends('layouts.app')

@section('css')
    <style>
        .form-group.required label:after {
            content: " *";
            color: red;
            font-weight: bold;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
            <h1>{{isset($products)?'Edit':'New'}} Product</h1>
            <hr/>
            @if(isset($products))
                {!! Form::model($products,['method'=>'put']) !!}
            @else
                {!! Form::open() !!}
            @endif
            <div class="form-group row required">
                {!! Form::label("name","Name",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                <div class="col-md-8">
                    {!! Form::text("name",null,["class"=>"form-control".($errors->has('name')?" is-invalid":""),"autofocus",'placeholder'=>'Name']) !!}
                    {!! $errors->first('name','<span class="invalid-feedback">:message</span>') !!}
                </div>
            </div>
            <div class="form-group row ">
                {!! Form::label("category1","category1",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                <div class="col-md-8">
                    {!! Form::select("category_1",$select,null,["class"=>"form-control"]) !!}
                </div>
            </div>
            <div class="form-group row ">
                    {!! Form::label("category1","category2",["class"=>"col-form-label col-md-3 col-lg-2"]) !!}
                    <div class="col-md-8">
                        {!! Form::select("category_2",$select,null,["class"=>"form-control"]) !!}
                    </div>
                </div>
            <div class="form-group row">
                <div class="col-md-3 col-lg-2"></div>
                <div class="col-md-4">
                    <a href="{{url('relation-category-product')}}" class="btn btn-danger">
                        Back</a>
                    {!! Form::button("Save",["type" => "submit","class"=>"btn
                btn-primary"])!!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection