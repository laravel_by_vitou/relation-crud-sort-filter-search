@extends('layouts.app')
@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet"/>
@endsection
@section('content')
    <div class="container">
        <div class="col-md-8 offset-md-2">
            <h1 style="font-size: 1.3rem;">Select2 load more function with laravel</h1>
            <hr/>
            <div class="form-group row required">
                {!! Form::label("country","Country",["class"=>"col-form-label col-md-2"]) !!}
                <div class="col-md-6">
                    {!! Form::select("country",[],null,["class"=>"form-control",'style'=>'width:100%']) !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#country').select2({
                ajax: {
                    url: '{{'select2-load-more'}}',
                    data: function (params) {
                        return {
                            search: params.term,
                            page: params.page || 1
                        };
                    },
                    dataType: 'json',
                    processResults: function (data) {
                        data.page = data.page || 1;
                        return {
                            results: data.items.map(function (item) {
                                return {
                                    id: item.id,
                                    text: item.name
                                };
                            }),
                            pagination: {
                                more: data.pagination
                            }
                        }
                    },
                    cache: true,
                    delay: 250
                },
                placeholder: 'Country',
//                minimumInputLength: 2,
                multiple: true
            });
        });
    </script>
@endsection