@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class=" d-flex justify-content-between">
                        <h3>Dashboard</h3>
                        <h4><a href="{{url('module/create')}}">Add</a></h4>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif You are logged as simeple user <strong> </strong>!
                    <hr>
                    <p class="text-muted">Modules</p>
                    <table class="table table-bordered bg-light color-light">
                        <thead class="">
                            <tr>
                                <th colspan="2">Practices
                                    <p></p>
                                </th>
                                <th class="text-center">References</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $i=1; 
                            @endphp 
                            @foreach( $modules as $module)
                            <tr>
                                <td>{{$i++}}</td>
                                <td align-center>
                                    <form class="row p-2" action="{{url('module/delete/'.$module->id)}}" method="post" id="{{$module->id}}">
                                        <a href="{{$module->p_title}}">{{$module->p_title}}</a> {{-- {{csrf_field()}} --}}
                                        <a href="{{url('module/update/'.$module->id)}}" class="btn btn-primary btn-sm mx-2">Edit</a>
                                        <input type="hidden" value="delete" name="_method">
                                        {{csrf_field()}}
                                        <a href="javascript:if(confirm('do you want to delete?')) $('#{{$module->id}}').submit()" class="btn btn-danger btn-sm mx-2">Delete</a>
                                    </form>
                                </td>
                                <td></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @if(count($modules))
                    <nav>
                        <ul class="pagination justify-content-end">
                            {{$modules->links('vendor.pagination.bootstrap-4')}}
                        </ul>
                    </nav>
                    @endif
                </div>
            </div>
        </div>
    </div>

</div>
@endsection