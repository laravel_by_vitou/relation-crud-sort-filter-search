<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::group(['/home','middleware'=>['web','auth']],function(){
    Route::get('/home','Homecontroller@index');
    
    // Add module in dashboard
    Route::group(['prefix'=>'module'],function(){
        Route::match(['get','post'],'create','HomeController@create');
        Route::match(['get','put'],'update/{id}','HomeController@update');
        Route::delete('/delete/{id}','HomeController@delete');

    });

    // select2
    Route::get('/select2-remote-data-source', 'Select2Controller@select2RemoteDataSource');
    Route::get('/select2-load-more', 'Select2Controller@select2LoadMore');  
    Route::match(['get', 'post'], 'test', function(){
        return 'test';
    });

    // laravel-crud-search-sort 

    Route::group(['prefix' => 'laravel-crud-search-sort'], function () {
        Route::get('/', 'Crud2Controller@index');
        Route::match(['get', 'post'], 'create', 'Crud2Controller@create');
        Route::match(['get', 'put'], 'update/{id}', 'Crud2Controller@update');
        Route::delete('delete/{id}', 'Crud2Controller@delete');
    });

    Route::group(['prefix'=>'my/laravel-crud-search-sort'],function(){
        Route::get('/','MyCrud2Controller@index');
        // Route::any('create','MyCrud2Controller@create');
        Route::match(['get','post'],'create','MyCrud2Controller@create');
    });

    // relationship
    Route::group(['prefix' => 'relation-many-to-many-category-product'], function () {
        Route::get('/', 'ProductsController@index');
        Route::match(['get', 'post'], 'create', 'ProductsController@create');
        Route::match(['get', 'put'], 'update/{id}', 'ProductsController@update');
        Route::delete('delete/{id}', 'ProductsController@delete');
    });

    

});
