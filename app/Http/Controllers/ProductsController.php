<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\User;
class ProductsController extends Controller
{
    function index()
    {
        $user_id = Auth()->user()->id;
        $products = User::find($user_id)->products()->orderBy('id','desc')->paginate(5);
        return view('relations/index',compact('products'));
    }

    function create(Request $request)
    {
        if($request->isMethod('get')){
            $categories = Category::orderBy('created_at','desc')->get();
            $select = array();
            foreach( $categories as $category)
            {
                $select[$category->id] = $category->name ;
            }
            return view('relations/form',compact('select'));
        }
        else
        {
            // return $request->all();exit;
            $product = New Product;
            $product->name = $request->name;
            $product->user_id = auth()->user()->id;
            $product->price = '40';
            $product->save();
            // Asign categories to a newly product
            $category = Category::find([$request->category_1,$request->category_2]);
            $product->categories()->attach($category);
            return redirect('/relation-many-to-many-category-product');
        }
    }
}
