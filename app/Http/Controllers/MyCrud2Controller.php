<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MyCutom;
class MyCrud2Controller extends Controller
{
    
    function index()
    {
        return view('my\crud2\index');
    }

    public function create( Request $request)
    {
        if( $request->isMethod('get'))
        {
            return view('my/crud2/form');
        }
        else
        {
            $rules = [
                'name'=>'required',
                'email'=>'required|email'
            ];
            $this->validate($request, $rules);
            
            // return redirect('my/laravel-crud-search-sort');
        }
    }
}
