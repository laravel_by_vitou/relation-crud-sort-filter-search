<?php

namespace App\Http\Controllers;


use App\Country;
use Illuminate\Http\Request;

class Select2Controller extends Controller
{
    public function select2RemoteDataSource()
    {
        return view('select2s/select2_remote_data_source');
    }

    public function select2LoadMore(Request $request)
    {
        $search = $request->get('search');
        $data = Country::select(['id', 'name'])->where('name', 'like', '%' . $search . '%')->orderBy('name')->paginate(5);
        return response()->json(['items' => $data->toArray()['data'], 'pagination' => $data->nextPageUrl() ? true : false]);
    }
}