<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Country;
use App\MyCustom;
use App\Dashboard;
use App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if( auth()->check() && auth()->user()->admin == 0)
        {
            $user_id = auth()->user()->id;
            $modules = User::find($user_id)
                ->dashboards()
                ->orderBy('id','desc')

                ->paginate(5);
            //  return $modules;
            return view('home',compact('modules'));
        }else{
            $user = User::all();
            return view('homeadmin')->with('users',$user);
        }
    }

    function create( Request $request)
    {
        if($request->isMethod('get')){
          return view('my/dashboards/form');
        }else{
            $rules=[
                'r_url'=>'nullable',
                'r_title'=>'nullable',
            ];

            $dashboard = new Dashboard();
            $dashboard->user_id = Auth()->user()->id;
            $dashboard->p_title = $request->p_title;
            $dashboard->p_url = $request->p_url;
            $dashboard->r_url = $request->r_url;
            $dashboard->r_title = $request->r_title;
            $dashboard->save();
            return redirect('/home');
        }
    }

    function update( Request $request, $id)
    {
        $user_id = auth()->user()->id;
        $did = Dashboard::find($id);
        if( $did == null)
        {
            return redirect('/home');
        }else{
            $uid = Dashboard::find($id)->user->id;
            if( $uid != $user_id )
            {
                return redirect('/home');
            }else{
                if($request->isMethod('get')){
                    return view('my/dashboards/form',['module'=>Dashboard::find($id)]);
                  }else{
                      $dashboard =  Dashboard::find($id);
                      $dashboard->user_id = Auth()->user()->id;
                      $dashboard->p_title = $request->p_title;
                      $dashboard->p_url = $request->p_url;
                      $dashboard->r_url = $request->r_url;
                      $dashboard->r_title = $request->r_title;
                      $dashboard->update();
                      
                      return redirect('/home');
                  }
            }
           
        }
        
    }

    function delete($id)
    {
        Dashboard::find($id)->delete();

        return redirect('/home');
    }
    
}
