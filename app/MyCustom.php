<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyCustom extends Model
{
    protected $table = 'my_customs';
}
