<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    function user()
    {
        return $this->belongsTo(User::class);
    }
}
