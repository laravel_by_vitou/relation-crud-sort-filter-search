<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Dashboard extends Model
{
    protected $table = 'dashboards';

    function user()
    {
        // return $this->belongsTo('App\User');
        return $this->belongsTo(User::class);
    }
}
